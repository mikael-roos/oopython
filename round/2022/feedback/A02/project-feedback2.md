I have reviewed your project submission in accordance with the requirements. I mainly perform the following procedure.

I look at the video to see how you present and show your project.

I review the README to see how you have documentet the game.

I run the game to try it out.

I try to execute 'make test' to check the code coverage and linter status.

I check the documentation you have generated.

I review the opposition reports you have received to see what the say about your projects.

I grade this submission as PASSED. The grade is set when you have submitted your opposition reports.

//Mikael

PS. Feel free to let me know if you have any questions or thoughts.
