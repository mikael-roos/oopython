Hi,

Your group is opposing/reviewing the following groups:

* A
* B

You find the projects in the zip file containing all projects, downloadable from here:
https://drive.google.com/file/d/1m1R6OQ6EuZmFuL4xSQY4A_OGytmpi7xo/view?usp=sharing

Do not spread the projects around, keep them safe.

The instruction for the opposing part is slightly updated at the bottom of this document:
https://hkr.instructure.com/courses/3641/pages/assignment-2-test-driven-development

//Mikael
