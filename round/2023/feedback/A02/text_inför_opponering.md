Hi,

I have quickly reviewed the parts of your code submission and it seems valid at a first glance.

Your group is opposing/reviewing 2 or 3 groups. If your group has one or two members, then oppose on at least two projects (Oppose 1, Oppose 2), you may also oppose on the optional (Oppose 3) if you feel like it. If your group has three members, then oppose on all three projects.

You can see the list of which projects you oppose on, including the link to the source code, here:

* https://docs.google.com/spreadsheets/d/1QyXKWlsp8KmOXBEytxMZcTAmGRqcVo3S4MNniHRs62s/edit?usp=sharing

The instruction for the opposing part is slightly updated at the bottom of this document:

* https://hkr.instructure.com/courses/5722/pages/assignment-2-test-driven-development

//Mikael
