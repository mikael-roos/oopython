Here are the reports from the opposition on your projects.

You should review them to see how the reviewers reflect on your project code, documentation and work environment and you should try learn from how they experienced your project.

It is always a good idea to try and listen to other when they are using your code.

Sometimes their thoughts will contain valuable feedback. One will just need to be able to understand what they are writing and try too see it from their perspective (and not "my own perspective").

This part of the assignment is DONE when you have read the reports.

//Mikael
