Assignment 3 - Sustainable programming through good and clean code
============================

You shall write an academic report on your views and experiences in the topic "sustainable programming through good and clean code". You write the report from your own viewpoint, experience and by gathering some data from you project in part 2.

This report will allow you to reflect on your learnings from the unittesting/TDD programming assignment.

This is an individual assignment and you must work on your own. You may discuss various issues with your peers and then write your own reflection on those discussions.



Size, format and structure
----------------------------

You should compose a report of 4-6 pages with txt, excluding the references page, the front page(s) and the table of content page.

You may write in English or Swedish.

Submit the final report as pdf.

The front page should contain your full name, email, student acronym, date of submission and the name of your education programme.

There is a report format called IMRAD that we will "sort of" follow. It provides a structure to the paper with the parts "Introduction, Methods, Results And Discussion" and we will use some of those parts. You can read more on [IMRAD on Wikipedia](https://en.wikipedia.org/wiki/IMRAD).

The structure of your report could be like this.

* Frontpage with title and details of the author
* Table of content
* Introduction
* Method
* Results
* Discussion
* Summary

The introduction provides the reader with the reasons for this report and put the report into context. Explain for the reader, the questions that this report tries to cover.

The method part describes how the study was done, on what bases and what typa of data was collected and how it was analysed.

Results are firm and objective results. That can be observations or results that one can "touch" like measurements and data.

Discussions are where we talk about the results and put them into context and try to understand what the results can or should mean.

In the summary we highlight what we find to be our most valuable findings.



Method
----------------------------

We are trying to look alike a short academic report and in that context a method is of high value. It shows what kind of study we did, what data we collected and how we went on with analysing it.

Our "method" could roughly be defined as follows.

* Observations from the group work in assignment 02
* Studies of the outcome of A02, the actual codebase
* Literature studies on unittesting, TDD and good code

As part of our studies of the codebase we will also collect raw data on software quality metrics doing the following.

* Measure the code size
* Measure the number of testobjects, testcases and code coverage
* Measure the number of linter errors
* Measure quality metrics using static code analysis



Requirements
----------------------------

Your report shall at least cover the following areas and topics.

* Elaborate and write on the functional programming style. Do you think it has some properties that relate to good and clean code?

* Elaborate and write on unittesting and explain your own view on that topic. What is unittesting, why do we do it. Is it hard, is it good, should one do it and does it relate to good code and if the answer is yes, then elaborate why may that be so?

* Elaborate and write on test driven development (TDD) and your experience and learnings from the coding assignment. Is it hard/easy/good/bad? Define what you think are the optimal environment for start using TDD and in what way may it be benificial for your code.

* Elaborate and write on your beliefs in documenting software. Is it needed, to what level is documentation needed and how should one do documentation to cover the aspects of good code.

* Elaborate on the topic of good and clean code, try to explian what it is and how should one proceed in learning how to write good and clean code.

* Elaborate and write on your thoughts on the importance of the programmers development environment and how important it might be, or not, for the programmers productivity in delivering good and clean code.

* Use software philosophies/principles (learnt from lecture) and relate those to your group work for A02. What principles did you use and what principles do you think you should have discussed to use in the project?

* Use static code analysis tools to measure your own project and the one you opposed on. Gather the metrics into the report and elaborate on how well the data is showing properties of good and clean code.

* Elaborate and write on the topic "sustainable software development", in your own view, what are the essentials?

You should have 4 to 8 references to sources you refer, cite or relate to in your text.



Report template
----------------------------

You may write in any program you choose. Here are a few suggestions.

There is a [report template with an HKR look alike view](https://drive.google.com/file/d/1FE_2ZCe4tB_Ha9GaXjQNNEusjQitXOPL/view?usp=sharing). You may want to use that as a template for your report.

You may want to consider LaTeX since it will help with your structure and references. It will also give you an academic look and feel of your report.

A popular online tool to use when writing LaTeX-documents are [Overleaf](https://www.overleaf.com/). It will allow you to cooperate with your team mates when writing the document.

Overleaf supplies an [extensive documentation](https://www.overleaf.com/learn/latex/Creating_a_document_in_LaTeX) which is useful when learning how to create LaTeX documents.

Here is an [example project in Overleaf](https://www.overleaf.com/read/jtsbvzptypjr), showing an outline of a sample report.



Grading
----------------------------

This assignment is graded as A-F. These are a few notes on how your submission is graded.



### Grade E, D

The report fulfills all requirements to a satisfactory level.

The report has a proper structure and the text is organised to be both readable and understandable.

The report has a flow of text and logic that makes sense and follows a red thread from start to end.



### Grade D, C

The report fulfills all requirements to a high level.

The report has a good structure and the text is organised to be both readable and understandable and there is a pleasant flow in the text.

The report has a flow of text and logic that makes sense and follows a red thread from start to end.

The results are clearly presented in the report and the discussion part is well structured where the reasoning is built upon the results.

The visible format and layout of the report appears to be both proper and pleasant to the eye.



### Grade B, A

The report fulfills all requirements to an excellent level of quality.

The report has a really good structure and the text is organised to be both readable and perfectly understandable and there is a really pleasant flow in the text.

The report has a flow of text and logic that makes sense and follows a red thread from start to end.

The results are clearly presented in the report and the discussion part is well structured where the reasoning is built upon the results.

The reasoning in the text is built upon several arguments and relates to both experience, data and references.

The visible format and layout of the report seems to be both proper and truly visual appealing to the eye.

This is a really good work that covers all the aspects of the requirements and the course topics.



Submission
----------------------------

1. Submit your report as pdf onto Canvas. Your submission will be submissed for plagiarism detection.

<!--
1. In the free text of the submission, write a final personal reflection on your TIL for the course.

1. Do also write a final note on the course and how you experienced it. This is your input to the teacher about the course. What was good/bad/educating/hard/easy/(not) important about the different tasks in the course. Feel free to suggests improvements and do even grade the course on a level 1 (not soo good course) to 10 (good course) realting to other courses you have taken.

What is a TIL? TIL is an acronym for "Today I Learned" which playfully indicates that there are always new things to learn, every day. You usually pick up things you have learned and where you might have hiked to a little extra about its usefulness or simplicity, or it was just a new lesson for the day that you want to note.
-->
