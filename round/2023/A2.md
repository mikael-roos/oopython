Assignment 2 - Test driven development
============================

In this assignment, you practice object-oriented coding in Python and focus on creating a proper development environment in Python for writing object-oriented programs with extensive use of unit testing and automated tools for reverse engineering to document the application.

You shall develop an object-oriented Python application, apply unit tests and document the final application.

You shall work in groups of 2 (at most 3) students.

The application you will build will most likely contain a set of classes of small or medium complexity. The important focus is to perform unit testing and try to create as good and clean code as possible.

You will use this project to evaluate if unit tests and a proper development environment can be a driving force to enhance your code quality.



Application idea
----------------------------

You shall develop a game of medium complexity with a handful of classes. The game is executed in the terminal and you will use the feature [cmd — support for line-oriented command interpreters](https://docs.python.org/3/library/cmd.html) to implement the game loop.

There are two game ideas from which you shall choose one to implement.



### Idea 1 - Pig (dice game)

Review the rules for the game at [Wikipedia on Pig (dice game)](https://en.wikipedia.org/wiki/Pig_(dice_game)). Decide what variation you want to implement.

Here are some suggestions for classes you might want to implement:

* Game, Dice, DiceHand, Histogram, Intelligence, HighScore, Player



### Idea 2 - War (card game)

Review the rules for the game at [Wikipedia on War (card game)](https://en.wikipedia.org/wiki/War_(card_game)). Decide what variation you want to implement.

Here are some suggestions for classes you might want to implement:

* Game, Card, Deck, CardHand, Histogram, Intelligence, HighScore, Player



Requirements
----------------------------

These are the requirements that must be met.



### Functionality

The game should be played by one player against the computer and as a two-player game.

Each player should be able to select a name for themself. A player should be able to change their name.

There should be a persistent high-score list where statistics on each player and their played games, are stored and visible. The statistics should remain if the player changes their name.

One should be able to see the rules of the game.

One should be able to play a whole game round. One should be able to quit the current game and restart it.

There should be a cheat that one can use for testing purposes and reach the end of the game faster.

The game should have a nice graphical (plain text or UTF-8 characters since we are in the terminal) representation so it looks fun to play it.

When the computer is playing, it should have some type of intelligence for playing the game. There might be several settings for the level of intelligence of the computer playing. These should be configurable by the user while playing.

Your game should be resilient and continue to work if/when the user enters bad input.

You should document the basics of the game in the project README.md. For example, you should describe how you choose to implement the intelligence part.



### Structure

Your application should be divided into classes where each class resides in its file. Use object-oriented programming style. Feel free to add a functional programming style where you feel it is suited.

Try hard to make small enough methods, good and clean code usually has smaller methods.

It would be really good if you store your work in a Git repo. Feel free to publish your project to a service like GitLab or GitHub. You can then use this repo when you submit your assignment results.

If you use Git you should have more than 50 commits and 10 tags. Treat it as a guideline.

Your project should contain a README.md that provides a description of the project and an instruction on how to install and run the game.

There should be a requirements.txt containing the essential packages to install to be able to run the application.

Feel free to use a Makefile to make it easier to build and test your project.

<!--
You shall have a file RELEASE.md where you document each release you do. Feel free to use Git tags for tagging your project.
-->

Add a LICENSE.md to your project.

Try to do your best to code in a pythonic manner and follow the PEP 20 guidelines.



### Unit testing

Your code shall be covered by unit tests.

The unit tests shall be stored in one file per class.

Each class should have a complete set of unit tests.

If you have modules and functions then these should also be covered by unit tests and these should be stored in their files so it is obvious which unit tests belong to a certain class or module.

Your README.md should contain a section on how to run the complete test suite and how to get the coverage report.

Feel free to implement `make test` and `make coverage` or something similar.

You shall measure your code coverage and you shall try to reach at least the following metrics.

* More than 90% code coverage.

* At least 10 test cases for each class.

* At least 20 assertions for each class.



### Document by comments

Your code must be well documented using python docstrings.



### Generate documentation from comments

It should be possible to generate documentation from your code and the python docstrings.

You should use an automated tool for this purpose. You may choose a tool of your liking.

Feel free to add a `make doc` target to solve this issue.

The documentation should be readable as HTML and included in your project below the `doc/api` directory.

Your README.md should contain details on how one can regenerate the documentation from your code.



### Generate UML diagrams from code

You should generate UML documentation from your code using a tool that uses reverse engineering on your code to create UML diagrams.

You should use an automated tool for this purpose. You may choose a tool of your liking.

Feel free to add a `make uml` target to solve this issue.

The documentation should be included in your project below the `doc/uml` directory. 

You should at least generate a class diagram and a package diagram.

Document in your README.md how to regenerate the UML diagrams of the documentation.



<!--
### Use semantic versioning

You shall use semantic versioning to document the releases of your application. Use git tags if you are using Git.

Always write the major releases into your RELEASE.md document with the version number, the release date and notes on what has changed.
-->


### Code style

You must add linters to ensure you have a proper code style and to avoid bad coding practices.

You must use at least the tools `pylint` and `flake8` with the `flake8-docstrings` and the `flake8-polyfill`.

Feel free to add `make lint` as a target to execute the linters.

Try to be as pythonic as you can, regarding the code style.

Try to avoid silencing the linter warning messages, and try to rewrite your code so it passes.

Try to avoid creating a "friendlier" configuration file for the linters. Try to be as hard and pythonic as possible.



### README.md

The grading of your game will start in the README.md. Ensure you use README.md to fully document your project and show how a new user can get going to both execute the application and know enough to work as a developer on your project.



Presentation video
----------------------

You are to record a video presentation of your project where you show how your project implements the features and fulfills the requirements. You should also present a few highlights from your source code and elaborate on why you feel the code is good/bad.

The video scene should contain a cam where your face is visible. Start your video by presenting yourself.

The tool OBS is a free and open-source tool you can use to record the presentation video.

The video should be 5-7 minutes.

You can save the video somewhere online and link to it. Services like YouTube provides for sharing videos by links that are not public.

Record one video per group.



Grading
----------------------------

This assignment is graded PASS or FAIL on a group basis. Individual grading might happen on a case-by-case basis whenever feel needed.

To achieve PASS the requirements should be implemented to a level of good quality on all levels.



Self-test
----------------------------

Before you submit you should (at least) perform this self-test to ensure your application is ready for delivery.

1. Can you install a fresh copy of the game from the zip-file/repo and run the game from the instructions in the README?
1. Does the project work when using bad inputs?
1. Does the project pass for code style and issues when using pylint?
1. Execute the unit tests and validate you have sufficient coverage.
1. Can you regenerate the documentation for the project?
1. Can you regenerate the UML documentation for the project?
1. Is there a LICENSE.md?
1. Does the project fulfill the overall requirements?

<!--
1. Is there a RELEASE.md and a LICENSE.md?
-->



Submission
----------------------------

1. Submit your code to Canvas as a zip file and name it `game.zip`. You may also add a link to the repo if you are using such a feature.

1. Record the presentation video and add a link to it on Canvas by using a comment on your original submission. You may do this the next day after the actual deadline.



Opposition
----------------------------

You will receive two projects from other student groups and your group shall analyze those project(s) and provide feedback of them.

In your review, you shall focus on performing the self-test explained above.

You should also provide answers to the following questions.

1. Play the game, what are your feeling about the overall quality of the game?
1. What do you think of the overall quality of the code and classes?
1. Anything more you would like to mention as feedback or improvement suggestions?

Write your summary in a pdf-document (approximately 2 pages). The pdf document should only contain the feedback of the project, you should keep it anonymous so no need to write your names in it.

Submit the documents to Canvas, one for each project you are reviewing, and name it the same name as the project you reviewed (A.pdf, B.pdf or equal).
