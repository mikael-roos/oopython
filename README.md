Course repo oopython
===========================

[![Documentation Status](https://readthedocs.org/projects/python-oo/badge/?version=latest)](https://python-oo.readthedocs.io/en/latest/?badge=latest)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Example program for object oriented Python with static code analyses, unit tests and documentation tools.

There is a [Question and Answer](questions-and-answers) area that might be of help to you.
