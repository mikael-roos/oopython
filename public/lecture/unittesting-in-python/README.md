---
author: mos
revision: 
    2023-01-03: "(A, mos) first version"
---
Unittesting in Python
====================

This is an introduction to unit testing in Python.

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/oopython/lecture/unittesting-in-python/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

<!--
* https://coverage.readthedocs.io/
* https://docs.python.org/3/library/unittest.html
-->
