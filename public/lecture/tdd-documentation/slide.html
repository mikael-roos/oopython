<!doctype html>
<html class="theme-5">
<meta charset="utf-8" />
<link href="../html-slideshow.bundle.min.css" rel="stylesheet" />
<link href="../style.css" rel="stylesheet" />
<script src="https://dbwebb.se/cdn/js/html-slideshow_v1.1.0.bundle.min.js"></script>

<title>TDD and Documentation</title>

<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# TDD and Documentation
## Work towards cleaner code
### Mikael Roos
</script>



<script data-role="slide" data-markdown type="text/html">
# Agenda

* Test driven development
* Documentation

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Test driven development
</script>



<script data-role="slide" type="text/html" data-markdown>
# Test-driven development

> Write your unit tests before you write the code.

> The code you write becomes testable.

> Your test suite becomes complete.

</script>



<script data-role="slide" type="text/html" data-markdown>
# The process

1. Write a unit test
1. Execute the test case and watch it fail
1. Write the smallest amount of code you possible can, to make the test case pass
1. Watch the test case pass
1. Refactor your code (while passing all tests)
1. Repeat

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/TDD_Global_Lifecycle.png" width="100%">
</figure>
</script>



<script data-role="slide" type="text/html" data-markdown>
# Principles usable in TDD

* Keep it simple, stupid (KISS)
* You aren't gonna need it (YAGNI)
* Fake it till you make it

</script>



<script data-role="slide" type="text/html" data-markdown>
# Additional philosophies

* Fix things early
* Never leave technical debt
* Heard of the broken window theory?

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/tech-debt.png" width="60%" title="https://kislayverma.com/wp-content/uploads/2021/07/tech-debt-1024x905.png">
</figure>
</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/broken-windows.jpeg" width="100%">
</figure>
</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# Documentation
</script>



<script data-role="slide" data-markdown type="text/html">
# Docs is?

* Cash is King
* Code is King
* Documentation is...?

</script>



<script data-role="slide" data-markdown type="text/html">
# Before or after?

> At first we need to understand the code to be built, create sketches.

> After the code is written we need to understand how it was built.

* Analysis & design (before)
    * Technical reports
    * Architecture
    * UML diagrams
* We need to know how to create the code and the architecture

</script>



<script data-role="slide" data-markdown type="text/html">
# Before or after?

> At first we need to understand the code to be built, create sketches.

> After the code is written we need to understand how it was built.

* Documentation (after)
    * Architecture
    * UML diagrams
    * Code documentation
* We need to know how to maintain and extend the code

</script>



<script data-role="slide" data-markdown type="text/html">
# Project documentation

* README.md
* CHANGELOG.md
* CONTRIBUTE.md
* LICENSE.md
* All included in the repo

</script>



<script data-role="slide" data-markdown type="text/html">
# README.md

* First point of entry to a project
* Describe the project
* How to install, setup and configure
* How to get going as a end user
* How to get going as a developer
* Frequent asked questions

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/readme.png" width="75%">
</figure>

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/readme-md.png" width="75%">
</figure>

</script>



<script data-role="slide" data-markdown type="text/html">
# Docstring

* PEP 257 - Docstring Conventions
* Add docstrings to code
* Lint that the docstrings are ok
* Generate documentation from docstrings

<p class="footnote">https://peps.python.org/pep-0257/</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/docstrings.png" width="60%">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# pdoc

* From docstrings to HTML

```
pdoc --force --html --output-dir doc/pdoc *.py 
```

```
make pdoc
```

<p class="footnote">https://pdoc.dev/</p>

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/pydoc-html.png" width="75%">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# UML

* Generate class diagram and package diagrams from source code
* Reverse engineering

```
pyreverse *.py -a1 -s1  

dot -Tpng classes.dot -o doc/pyreverse/classes.png   
dot -Tpng packages.dot -o doc/pyreverse/packages.png 
```

```
make pyreverse
```

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/pyreverse-packages.png" width="40%">
</figure>
</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/pyreverse-classes.png" width="80%">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Docs in repo

Keep a `docs/` in your repo.

* Continuous Documentation Deployment
* Add user/developer documentation to the repo
    * Keep in sync with the code version
* Integrate with a service like "ReadTheDocs"
* On each commit, your documentation is rebuilt from the source in your repo
    * Or build locally

<p class="footnote">https://about.readthedocs.com/</p>

</script>



<script data-role="slide" data-markdown type="text/html">
# Read the docs

Documentation source and tools to build the docs.

```
$ tree docs
docs
├── Makefile
├── build
├── make.bat
└── source
    ├── api.rst
    ├── conf.py
    ├── generated
    ├── index.rst
    └── usage.rst
```

```
make install html
```

</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/index-rst.png" width="75%">
</figure>
</script>



<script data-role="slide" type="text/html" data-markdown class="center full">
<figure>
<img src="img/buildthedocs.png" width="75%">
</figure>
</script>



<script data-role="slide" data-markdown type="text/html">
# Automate it

* Automate the documentation to make it easy
* Apply "Continuous Documentation Deployment"
* Keep in sync with the code version

```
make doc
```

* Apply TDD to documentation, documentation driven development
    * Write documentation before the code

</script>



<script data-role="slide" type="text/html" data-markdown>
# Summary

* TDD
* Documentation

</script>



<script data-role="slide" type="text/html" data-markdown class="titlepage center">
# The End
</script>



<script data-role="slide" data-markdown type="text/html">
</script>
