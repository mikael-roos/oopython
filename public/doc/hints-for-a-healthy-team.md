---
revision:
    "2023-01-04": "(A, mos) First version."
---
Hints for a healthy team
========================

This is a collection of hints that may make your teamwork go smoother.

[[_TOC_]]



Project template
------------------------

There is a project template with a structure including a Makefile, a sample of code with unit tests and all tools included. The template is documented in its README.md so you can easily clone it and work through all the examples to see how it works.

* [Python development project template](https://gitlab.com/mikael-roos/python-template)

Ensure that each project member can download and work on the project template.



Prepare yourself
------------------------

When all are prepared, have your first project meeting.

1. Make a health check, ask all members if they feel okay with the setup and check that they are onboard and ready.
1. Let all members tell "what they can bring to the table" and if they have any special expectations on the project work.

Walk through the assignment spec and discuss it.

1. Prioritize features if needed and decide on your project focus.
1. Do a rough sketch of the class design (UML).
1. Roughly try to divide the work (you do that class and you do that class). You can change this as the project goes on.
1. Decide on a project structure (use the suggested project template?).
1. Decide on how to share your code, will you use Git or not?
1. Appoint a Git manager or someone who will be responsible to integrate all the members' code into a working version.

If you go with Git.

1. Set up the Git repository, and put it onto GitHub.
1. Ensure that all team members can clone the code (so they can get a fresh copy of the code).
1. Decide if all members should have the right to push their code to the repo (otherwise one person needs to integrate the code manually).

Start to work.

1. Make a `main.py` and you are on your way.
1. Integrate your code at least once a week to avoid integration problems.



Work weekly
------------------------

Always have a checkup meeting each week, it is important to communicate.

Always try to merge your code and have a working application each Friday. That will show that you are on the right path and you will minimize trouble in the delivery week.

Here is a rough weekly plan for your project.

* Week 1: Try to have some code written already by the first week. If you have that - then it will be a great start for unit testing.

* Week 2: Add unit tests to your code. All classes should have unit tests. Continue to work and integrate.

* Week 3: Finalize the code and generate documentation. Prepare to deliver.

* Week 4: Deliver
